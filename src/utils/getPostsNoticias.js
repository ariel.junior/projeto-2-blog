const getPostsNoticias = setState => {
    const _listaPostsNoticias = [
        {
            'id': 1,
            'imagem':
                "https://st.depositphotos.com/1780879/3816/i/600/depositphotos_38166573-stock-photo-trees-with-sunbeams.jpg",
            'titulo': 
                'Postagem: Advenços da Tecnologia',

            'texto': 
                'Durante esses últimos tempos, a tecnologia '
                + 'vem sofrendo avanços...',

            'atualizacao':
                'Última atualização 15 dias atrás'
        },
        {
            'id': 2,
            'imagem':
                "https://st.depositphotos.com/1780879/3816/i/600/depositphotos_38166573-stock-photo-trees-with-sunbeams.jpg",
            'titulo': 
                'Postagem2: Advenços da Tecnologia',

            'texto': 
                'Durante esses últimos tempos, a tecnologia '
                + 'vem sofrendo avanços...',

            'atualizacao':
                'Última atualização 15 dias atrás'
        },
        {
            'id': 3,
            'imagem':
                "https://st.depositphotos.com/1780879/3816/i/600/depositphotos_38166573-stock-photo-trees-with-sunbeams.jpg",
            'titulo': 
                'Postagem3: Advenços da Tecnologia',

            'texto': 
                'Durante esses últimos tempos, a tecnologia '
                + 'vem sofrendo avanços...',

            'atualizacao':
                'Última atualização 15 dias atrás'
        },
        {
            'id': 4,
            'imagem':
                "https://st.depositphotos.com/1780879/3816/i/600/depositphotos_38166573-stock-photo-trees-with-sunbeams.jpg",
            'titulo': 
                'Postagem4: Advenços da Tecnologia',

            'texto': 
                'Durante esses últimos tempos, a tecnologia '
                + 'vem sofrendo avanços...',

            'atualizacao':
                'Última atualização 15 dias atrás'
        },
        {
            'id': 5,
            'imagem':
                "https://st.depositphotos.com/1780879/3816/i/600/depositphotos_38166573-stock-photo-trees-with-sunbeams.jpg",
            'titulo': 
                'Postagem5: Advenços da Tecnologia',

            'texto': 
                'Durante esses últimos tempos, a tecnologia '
                + 'vem sofrendo avanços...',

            'atualizacao':
                'Última atualização 15 dias atrás'
        }
    ]

    setState(_listaPostsNoticias)
}

export default getPostsNoticias